@echo off

rem      *************
rem      * Constants *
rem      *************

set THIS_DIR=%~dp0
set SRC_DIR=%THIS_DIR%\src
set PROJECTS_ROOT_DIR=%THIS_DIR%\..\..
rem FIXME: Configurable git path
set GIT="C:\Program Files\Git\bin\git.exe"



rem      ****************
rem      * Version info *
rem      ****************

for /f "delims=" %%a in ('%GIT% -C %THIS_DIR% describe --always --dirty') do set version=%%a

set version_dir=%THIS_DIR%\..\%version%



rem      ************
rem      * Defaults *
rem      ************

set hcd_arch=amd64
set build=true
set optimise=false
set clean=false



rem      *******************
rem      * Parse Arguments *
rem      *******************

:label_parse_arguments

if [%1]==[] (
  goto label_no_further_arguments
)

set argument=%1
shift

if [%argument:~0,1%]==[/] (
  set argument=%argument:~1%
) else if [%argument:~0,2%]==[--] (
  set argument=%argument:~2%
) else (
  echo WARNING - Ignoring unexpected argument: %argument%
  goto label_parse_arguments
)


 if [%argument%]==[arch] (
  goto set_arch
) else if [%argument%]==[optimise] (
  set optimise=true
) else if [%argument%]==[clean] (
  set clean=true
) else if [%argument%]==[no-build] (
  set build=false
) else (
  echo WARNING - unknown option ignored: /%argument%
)

goto label_parse_arguments


:set_arch
shift
if [%1]==[] (
  echo WARNING: arch unspecified!
) else (
  set hcd_arch=%1
  shift
)
goto label_parse_arguments


:label_no_further_arguments



rem      *****************
rem      * Load MSVC Env *
rem      *****************

set MSVC_ENV_TOOL_DIR=%PROJECTS_ROOT_DIR%\msvcEnv\tool

call %MSVC_ENV_TOOL_DIR%\msvcEnv.bat %hcd_arch%

if %ERRORLEVEL% neq 0 (
  echo Could not load msvc env for arch: %hcd_arch%
  echo Aborting...
  exit /B 1
)



rem      ***********
rem      * Defines *
rem      ***********

set defines=

if [%optimise%]==[false] (
  set defines=%defines% -DHCD_ENABLE_SLOW_ASSERTS
)



rem      ****************
rem      * Source Files *
rem      ****************

set includes=-I %SRC_DIR%
set src_files=%SRC_DIR%\imgui_libMain.cpp

if [%optimise%]==[true] (
  set obj_file=imgui_optimised.obj
) else (
  set obj_file=imgui.obj
)



rem      ************
rem      * Packages *
rem      ************

set includes=

set includes=%includes% -I %THIS_DIR%\common\pkg\src
for /f "delims=" %%a in ('%GIT% -C %THIS_DIR%\common\pkg describe --always --dirty') do set common_pkg_ver=%%a

set includes=%includes% -I %THIS_DIR%\gl3w\pkg\src
for /f "delims=" %%a in ('%GIT% -C %THIS_DIR%\gl3w\pkg describe --always --dirty') do set gl3w_pkg_ver=%%a

set includes=%includes% -I %THIS_DIR%\openglGame\common\pkg\src
for /f "delims=" %%a in ('%GIT% -C %THIS_DIR%\openglGame\common\pkg describe --always --dirty') do set openglGame_common_pkg_ver=%%a

set includes=%includes% -I %THIS_DIR%\imgui\adaptor\pkg\src
for /f "delims=" %%a in ('%GIT% -C %THIS_DIR%\imgui\adaptor\pkg describe --always --dirty') do set imgui_adaptor_pkg_ver=%%a

set includes=%includes% -I %THIS_DIR%\imgui\external\pkg
for /f "delims=" %%a in ('%GIT% -C %THIS_DIR%\imgui\external\pkg describe --always --dirty') do set imgui_external_pkg_ver=%%a


set headers=%THIS_DIR%\imgui\adaptor\pkg\src\ImGui_Adaptor.hpp ^
            %THIS_DIR%\imgui\external\pkg\imgui.h ^
            %THIS_DIR%\imgui\external\pkg\imconfig.h



rem      *************
rem      * Libraries *
rem      *************

set libraries=



rem      ******************
rem      * Warnings Flags *
rem      ******************

set warning_flags=

rem treat warnings as errors
set warning_flags=%warning_flags% /WX

rem enable level 4 (almost all) warnings
set warning_flags=%warning_flags% /W4

rem disable "unreferenced formal parameter"
set warning_flags=%warning_flags% /wd4100

rem disable "conditional expression is constant"
set warning_flags=%warning_flags% /wd4127

rem allow zero-sized array in struct/union
set warning_flags=%warning_flags% /wd4200

rem allow nameless structs and unions
set warning_flags=%warning_flags% /wd4201

rem disable "conversion from 'type1' to 'type2', possible loss of data"
set warning_flags=%warning_flags% /wd4244

rem disable "conversion from 'type1' to pointer of greater size"
set warning_flags=%warning_flags% /wd4312

rem disable "declaration of argument hides class member"
set warning_flags=%warning_flags% /wd4458

rem disable "unreferenced local function has been removed"
set warning_flags=%warning_flags% /wd4505

rem disable "termination on exception is not guaranteed"
set warning_flags=%warning_flags% /wd4577

rem disable security warnings
set warning_flags=%warning_flags% -D_CRT_SECURE_NO_WARNINGS



rem      *******************
rem      * Optimiser Flags *
rem      *******************

set optimiser_flags=

if [%optimise%]==[true] (
  rem enable optimisation
  set optimiser_flags= %optimiser_flags% /O2
) else (
  rem disable optimisation
  set optimiser_flags= %optimiser_flags% /Od
)

rem allow optimisation of floating point operations
set optimiser_flags= %optimiser_flags% /fp:fast

rem generate intrinsics
set optimiser_flags= %optimiser_flags% /Oi



rem      ******************
rem      * Compiler Flags *
rem      ******************

set compiler_flags= %includes% ^
                    %defines% ^
                    %optimiser_flags% ^
                    %warning_flags%

rem compile only
set compiler_flags=%compiler_flags% /c

rem disable output of compiler version info
set compiler_flags=%compiler_flags% /nologo

rem full path of source files in diagnostics
set compiler_flags=%compiler_flags% /FC

rem full path of source files in diagnostics
set compiler_flags=%compiler_flags% /FC

rem embed debug symbols in .obj
set compiler_flags=%compiler_flags% /Z7

rem better debug when optimised
set compiler_flags=%compiler_flags% /Zo

rem disable minimal rebuild
set compiler_flags=%compiler_flags% /Gm-

rem disable RTTI
set compiler_flags=%compiler_flags% /GR-

rem disable exceptions
set compiler_flags=%compiler_flags% /EHa-



rem      *********************
rem      * C Runtime Library *
rem      *********************

if 1 == 1 (
  rem Use msvcrt.lib & msvcrXXX.dll
  set compiler_flags=%compiler_flags% /MD
  set linker_flags=%linker_flags% /NODEFAULTLIB:libc.lib /NODEFAULTLIB:libcmt.lib /NODEFAULTLIB:libcd.lib /NODEFAULTLIB:libcmtd.lib /NODEFAULTLIB:msvcrtd.lib
) else (
  rem Use libcmt.lib
  rem FIXME: doesn't work - missing some libs?
  set compiler_flags=%compiler_flags% /MT
  set linker_flags=%linker_flags% /NODEFAULTLIB:libc.lib /NODEFAULTLIB:msvcrt.lib /NODEFAULTLIB:libcd.lib /NODEFAULTLIB:libcmtd.lib /NODEFAULTLIB:msvcrtd.lib
)



rem      *********
rem      * Clean *
rem      *********

if [%clean%]==[true] (
  echo Clean currently not supported
  echo Aborting...
  exit /B 1
)



rem      *********
rem      * Build *
rem      *********

if [%build%]==[true] (
  if not exist %version_dir% mkdir %version_dir%
  pushd %version_dir%

  if not exist include mkdir include
  for %%f in (%headers%) do copy /Y %%f include > NUL 2> NUL

  if not exist pkg mkdir pkg
  echo %common_pkg_ver%>pkg\common
  echo %gl3w_pkg_ver%>pkg\gl3w
  echo %openglGame_common_pkg_ver%>pkg\openglGame_common
  echo %imgui_adaptor_pkg_ver%>pkg\imgui_adaptor
  echo %imgui_external_pkg_ver%>pkg\imgui_external

  if not exist %hcd_arch% mkdir %hcd_arch%
  cd %hcd_arch%
  if not exist %hcd_platform% mkdir %hcd_platform%
  cd %hcd_platform%

  del %obj_file% > NUL 2> NUL

  rem compile!
  cl %compiler_flags% ^
     %src_files% ^
     /Fmmap.txt ^
     /Fo%obj_file%

  lib /nologo %obj_file%
  popd
)
